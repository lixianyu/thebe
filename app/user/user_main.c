/*
 * ESPRESSIF MIT License
 *
 * Copyright (c) 2016 <ESPRESSIF SYSTEMS (SHANGHAI) PTE LTD>
 *
 * Permission is hereby granted for use on ESPRESSIF SYSTEMS ESP8266 only, in which case,
 * it is free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "osapi.h"
#include "at_custom.h"
#include "user_interface.h"
#include "ets_sys.h"
#include "driver/uart.h"

#include "mqtt.h"
#include "wifi.h"
#include "config.h"
#include "debug.h"
#include "gpio.h"
#include "mem.h"

#define SW_VERSION "0.0.620.00"


extern void at_exeCmdCiupdate(uint8_t id);
void Thebe_at_mqtt_init(void);


MQTT_Client mqttClient;
uint8 gIfSetDevIDAndToken = 0;
uint8 gIfSetMqttServerPort = 0;
uint8 gIfEverDisconnected = 0;
char *gDefaultTopic = NULL;
char *gDefaultMessage = NULL;
#define TOPIC_NUMBER 10
char *gSubTopics[TOPIC_NUMBER] = {NULL};

// test :AT+TEST=1,"abc"<,3>
void ICACHE_FLASH_ATTR
at_setupCmdTest(uint8_t id, char *pPara)
{
    int result = 0, err = 0, flag = 0;
    uint8 buffer[32] = {0};
    pPara++; // skip '='

    //get the first parameter
    // digit
    flag = at_get_next_int_dec(&pPara, &result, &err);

    // flag must be ture because there are more parameter
    if (flag == FALSE) {
        at_response_error();
        return;
    }

    if (*pPara++ != ',') { // skip ','
        at_response_error();
        return;
    }

    os_sprintf(buffer, "the first parameter:%d\r\n", result);
    at_port_print(buffer);

    //get the second parameter
    // string
    at_data_str_copy(buffer, &pPara, 10);
    at_port_print("the second parameter:");
    at_port_print(buffer);
    at_port_print("\r\n");

    if (*pPara == ',') {
        pPara++; // skip ','
        result = 0;
        //there is the third parameter
        // digit
        flag = at_get_next_int_dec(&pPara, &result, &err);
        // we donot care of flag
        os_sprintf(buffer, "the third parameter:%d\r\n", result);
        at_port_print(buffer);
    }

    if (*pPara != '\r') {
        at_response_error();
        return;
    }

    at_response_ok();
}

void ICACHE_FLASH_ATTR
at_testCmdTest(uint8_t id)
{
    uint8 buffer[32] = {0};

    os_sprintf(buffer, "%s\r\n", "at_testCmdTest");
    at_port_print(buffer);
    at_response_ok();
}

void ICACHE_FLASH_ATTR
at_queryCmdTest(uint8_t id)
{
    uint8 buffer[32] = {0};

    os_sprintf(buffer, "%s\r\n", "at_queryCmdTest");
    at_port_print(buffer);
    at_response_ok();
}
void ICACHE_FLASH_ATTR
at_queryCmdLxy(uint8_t id)
{
    uint8 buffer[32] = {0};

    os_sprintf(buffer, "%s\r\n", "at_queryCmdLxy");
    at_port_print(buffer);
    at_response_ok();
}

void ICACHE_FLASH_ATTR
at_exeCmdLxy(uint8_t id)
{
    uint8 buffer[32] = {0};
    os_sprintf(buffer, "%s\r\n", "at_exeCmdLxy");
    at_port_print("Connectting MQTT server...");
    //MQTT_Connect(&mqttClient);
    at_response_ok();
}

void ICACHE_FLASH_ATTR
at_exeCmdTest(uint8_t id)
{
    uint8 buffer[32] = {0};

    os_sprintf(buffer, "%s\r\n", "at_exeCmdTest_lxy");
    at_port_print(buffer);
    
    os_sprintf(buffer,"compile time:%s %s\r\n",__DATE__,__TIME__);
    at_port_print(buffer);
    
    os_sprintf(buffer, "SDK version:%s\r\n", system_get_sdk_version());
    at_port_print(buffer);
    
    uint32 chipID = system_get_chip_id();
    //gu32ChipID = chipID;
    //gu32ChipID1 = chipID;
    os_sprintf(buffer, "chip id = %d\r\n", chipID);
    at_port_print(buffer);
    
    system_print_meminfo();
    uint32 freeheap = system_get_free_heap_size();
    //gu32 = freeheap;
    os_sprintf(buffer, "free heap size = %d bytes\r\n", freeheap);
    at_port_print(buffer);
    
    uint8 cfq = system_get_cpu_freq();
    os_sprintf(buffer, "cpu freq = %dMHz\r\n", cfq);
    at_port_print(buffer);
    //os_printf_plus(buffer);
    
    //at_port_print(buffer);
    at_response_ok();
}

void ICACHE_FLASH_ATTR
at_setupCmdMqtsetser(uint8_t id, char *pPara)
{
    int result = 0, err = 0, flag = 0;
    uint8 buffer[128] = {0};
    pPara++; // skip '='

    int32 lens = at_data_str_copy(buffer, &pPara, 128);
#if 0
    at_port_print("the first parameter:");
    at_port_print(buffer);
    at_port_print("\r\n");
#else
    os_strncpy(sysCfg.mqtt_host, buffer, 128);
#endif
    if (lens == -1 || *pPara++ != ',') { // skip ','
        at_port_print("Usage: AT+MQTSETSER=\"server\",port");
        at_response_error();
        return;
    }
    //get the second parameter
    // digit
    flag = at_get_next_int_dec(&pPara, &result, &err);
#if 0
    os_sprintf(buffer, "the second parameter:%d, flag = %d, err = %d\r\n", result, flag, err);
    at_port_print(buffer);
#endif
    if (*pPara != '\r' || flag != 0 ) {
        at_port_print("Usage: AT+MQTSETSER=\"server\",port");
        at_response_error();
        return;
    }
    sysCfg.mqtt_port = result;
    sysCfg.security = DEFAULT_SECURITY;
    MQTT_InitConnection(&mqttClient, sysCfg.mqtt_host, sysCfg.mqtt_port, sysCfg.security);
    at_response_ok();
    gIfSetMqttServerPort = 1;
}

// AT+MQTCON="id","user","pass"
void ICACHE_FLASH_ATTR
at_setupCmdMqtcon(uint8_t id, char *pPara)
{
    int result = 0, err = 0, flag = 0;
    uint8 buffer[128] = {0};

    if (mqttClient.connState == MQTT_DATA)
    {
        os_strncpy(buffer, "Already connected.\r\n", sizeof(buffer));
        at_port_print(buffer);
        at_response_ok();
        return;
    }
    if (gIfSetMqttServerPort == 0)
    {
        at_port_print("Please set server and port first.\r\n");
        at_response_error();
        return;
    }
    
    pPara++; // skip '='

    sysCfg.mqtt_keepalive = MQTT_KEEPALIVE;
    int32 lens = at_data_str_copy(buffer, &pPara, sizeof(sysCfg.device_id));
    #if 0
    at_port_print("the first parameter:");
    at_port_print(buffer);
    at_port_print("\r\n");
    #endif
    os_memset(sysCfg.device_id, 0, sizeof(sysCfg.device_id));
    os_strncpy(sysCfg.device_id, buffer, sizeof(sysCfg.device_id) - 1);
    
    if (lens == -1 || *pPara++ != ',') { // skip ','
        at_port_print("Usage: AT+MQTCON=\"device_id\",\"user_name\",\"token\"");
        at_response_error();
        return;
    }
    lens = at_data_str_copy(buffer, &pPara, sizeof(sysCfg.mqtt_user));
    #if 0
    at_port_print("the second parameter:");
    at_port_print(buffer);
    at_port_print("\r\n");
    #endif
    os_memset(sysCfg.mqtt_user, 0, sizeof(sysCfg.mqtt_user));
    os_strncpy(sysCfg.mqtt_user, buffer, sizeof(sysCfg.mqtt_user) - 1);
    
    if (lens == -1 || *pPara++ != ',') { // skip ','
        at_port_print("Usage: AT+MQTCON=\"device_id\",\"user_name\",\"token\"");
        at_response_error();
        return;
    }
    if (*pPara != '"') {
        at_port_print("Usage: AT+MQTCON=\"device_id\",\"user_name\",\"token\"");
        at_response_error();
        return;
    }

    lens = at_data_str_copy(buffer, &pPara, sizeof(sysCfg.mqtt_pass));
    if (lens == -1 || *pPara != '\r')
    {
        at_port_print("Usage: AT+MQTCON=\"device_id\",\"user_name\",\"token\"");
        at_response_error();
        return;
    }
    #if 0
    char tmp[64] = {0};
    os_sprintf(tmp, "the third parameter:%d, len:%d", buffer, lens);
    at_port_print(tmp);
    at_port_print("\r\n");
    #endif
    os_memset(sysCfg.mqtt_pass, 0, sizeof(sysCfg.mqtt_pass));
    os_strncpy(sysCfg.mqtt_pass, buffer, sizeof(sysCfg.mqtt_pass) - 1);

    MQTT_InitClient(&mqttClient, sysCfg.device_id, sysCfg.mqtt_user, sysCfg.mqtt_pass, sysCfg.mqtt_keepalive, 0);

    Thebe_at_mqtt_init();
    MQTT_Connect(&mqttClient);
    //at_response_ok();
}

void ICACHE_FLASH_ATTR
at_exeCmdMqtcon(uint8_t id)
{
    char buffer[128] = {0};
    if (mqttClient.connState == MQTT_DATA)
    {
        os_strncpy(buffer, "Already connected.\r\n", sizeof(buffer));
        at_port_print(buffer);
        at_response_ok();
        return;
    }
    if (gIfSetMqttServerPort == 0)
    {
        at_port_print("Please set server and port first.\r\n");
        at_response_error();
        return;
    }
    if (gIfSetDevIDAndToken == 0)
    {
        at_port_print("Please set Device ID and Token first.\r\n");
        at_response_error();
        return;
    }
    sysCfg.mqtt_keepalive = MQTT_KEEPALIVE;
    MQTT_InitClient(&mqttClient, sysCfg.device_id, sysCfg.mqtt_user, sysCfg.mqtt_pass, sysCfg.mqtt_keepalive, 1);
    Thebe_at_mqtt_init();
    MQTT_Connect(&mqttClient);
}

char * get_a_unused_topic(void)
{
    int i;
    for (i = 0; i < TOPIC_NUMBER; i++)
    {
        if (gSubTopics[i] != NULL)
        {
            continue;
        }
        else
        {
            break;
        }
    }
    if (i >= TOPIC_NUMBER)
    {
        return NULL;
    }

    gSubTopics[i] = (char*)os_zalloc(128);
    memset(gSubTopics[i], 0, 128);
    return gSubTopics[i];
}
    
// AT+MQTSUBS="topic"
void ICACHE_FLASH_ATTR
at_setupCmdMqtsubs(uint8_t id, char *pPara)
{
    int result = 0, err = 0, flag = 0;
    char buffer[128] = {0};
    pPara++; // skip '='
    at_data_str_copy(buffer, &pPara, 127);

    MQTT_Subscribe(&mqttClient, buffer, 0);
    at_response_ok();

    char *topic = get_a_unused_topic();
    if (topic != NULL)
    {
        strncpy(topic, buffer, 127);
    }
}

void ICACHE_FLASH_ATTR
at_setupCmdMqtpub(uint8_t id, char *pPara)
{
    #define AT_MAX_LEN 116
    int result = 0, err = 0, flag = 0;
    char *topicBuf = (char*)os_zalloc(128),
         *dataBuf = (char*)os_zalloc(1024);
    pPara++; // skip '='
    os_memset(topicBuf, 0, 128);
    int32 lenTopic = at_data_str_copy(topicBuf, &pPara, 127);
    //int lenTopic = os_strlen(topicBuf);
    #if 0
    at_port_print("the first parameter:");
    at_port_print(topicBuf);
    at_port_print("\r\n");
    #endif

    if (*pPara++ != ',') { // skip ','
        at_response_error();
        os_free(topicBuf);
        os_free(dataBuf);
        return;
    }
    pPara++; // skip "

    int32 leftLen = AT_MAX_LEN - lenTopic - 2;
    if (leftLen <= 0)
    {
        at_response_error();
        os_free(topicBuf);
        os_free(dataBuf);
        return;
    }
    os_memset(dataBuf, 0, 1024);
    int32 i = 0;
    while (*pPara != '\r')
    {
        dataBuf[i] = *pPara;
        i++;
        if (i > 1024)
        {
            break;
        }
        pPara++;
    }
    //int32 len = at_data_str_copy(dataBuf, &pPara, 1024);
    int32 len = i - 1;
    dataBuf[len] = 0;
    #if 0
    at_port_print("the second parameter:");
    at_port_print(dataBuf);
    at_port_print("\r\n");
    #endif
#if 0
    if (*pPara != '\r') {
        at_response_error();
        os_free(topicBuf);
        os_free(dataBuf);
        return;
    }
#endif
    MQTT_Publish(&mqttClient, topicBuf, dataBuf, len, 0, 0);
    //at_response_ok();
    os_free(topicBuf);
    os_free(dataBuf);
}

void ICACHE_FLASH_ATTR
at_exeCmdMqtpub(uint8_t id)
{
    int len = os_strlen(gDefaultMessage);
    MQTT_Publish(&mqttClient, gDefaultTopic, gDefaultMessage, len, 0, 0);
}

void ICACHE_FLASH_ATTR
at_setupCmdMqtpubm(uint8_t id, char *pPara)
{
    #define AT_MAX_LEN 116
    int result = 0, err = 0, flag = 0;
    char *dataBuf = (char*)os_zalloc(1024);
    char *topicBuf = (char*)os_zalloc(128);
    pPara++; // skip '='
    pPara++; // skip "

    int32 leftLen = AT_MAX_LEN - 2;
    if (leftLen <= 0)
    {
        at_response_error();
        os_free(topicBuf);
        os_free(dataBuf);
        return;
    }
    os_memset(dataBuf, 0, 1024);
    int32 i = 0;
    while (*pPara != '\r')
    {
        dataBuf[i] = *pPara;
        i++;
        if (i > 1024)
        {
            break;
        }
        pPara++;
    }
    int32 len = i - 1;
    dataBuf[len] = 0;
    #if 0
    at_port_print("the first parameter:");
    at_port_print(dataBuf);
    at_port_print("\r\n");
    #endif
    os_sprintf(topicBuf, "dp/%s", sysCfg.device_id);
    MQTT_Publish(&mqttClient, topicBuf, dataBuf, len, 0, 0);
    os_free(topicBuf);
    os_free(dataBuf);
}

void ICACHE_FLASH_ATTR
at_exeCmdMqtdiscon(uint8_t id)
{
    if (mqttClient.connState == TCP_DISCONNECTED)
    {
        at_response_ok();
        return;
    }
    MQTT_Disconnect(&mqttClient);
    //at_response_ok();
}

void ICACHE_FLASH_ATTR
at_queryCmdMqtstate(uint8_t id)
{
    uint8 buffer[32] = {0};
    
    os_sprintf(buffer, "Connect state : %d\r\n", mqttClient.connState);
    at_port_print(buffer);
    at_response_ok();
}

void ICACHE_FLASH_ATTR
at_queryCmdMqtcond(uint8_t id)
{
    uint8 buffer[32] = {0};
    if (mqttClient.connState == MQTT_DATA)
    {
        os_strncpy(buffer, "CONN1", sizeof(buffer));
    }
    else
    {
        os_strncpy(buffer, "CONN0", sizeof(buffer));
    }
    at_port_print(buffer);
    at_response_ok();
}

#if 0
void ICACHE_FLASH_ATTR
at_setupCmdMqtidtoken(uint8_t id, char *pPara)
{
    #define AT_MAX_LEN 116
    int result = 0, err = 0, flag = 0;
    char *topicBuf = (char*)os_zalloc(128),
         *dataBuf = (char*)os_zalloc(1024);
    pPara++; // skip '='
    os_memset(topicBuf, 0, 128);
    int32 lenTopic = at_data_str_copy(topicBuf, &pPara, 127);
    //int lenTopic = os_strlen(topicBuf);
    #if 1
    at_port_print("the first parameter:");
    at_port_print(topicBuf);
    at_port_print("\r\n");
    #endif
    os_memset(sysCfg.device_id, 0, sizeof(sysCfg.device_id));
    os_strncpy(sysCfg.device_id, topicBuf, sizeof(sysCfg.device_id));
    if (*pPara++ != ',') { // skip ','
        at_response_error();
        os_free(topicBuf);
        os_free(dataBuf);
        return;
    }
    pPara++; // skip "

    int32 leftLen = AT_MAX_LEN - lenTopic - 2;
    if (leftLen <= 0)
    {
        at_response_error();
        os_free(topicBuf);
        os_free(dataBuf);
        return;
    }
    os_memset(dataBuf, 0, 1024);
    int32 i = 0;
    while (*pPara != '\r')
    {
        dataBuf[i] = *pPara;
        i++;
        if (i > 1024)
        {
            break;
        }
        pPara++;
    }
    //int32 len = at_data_str_copy(dataBuf, &pPara, 1024);
    int32 len = i - 1;
    dataBuf[len] = 0;
    #if 1
    at_port_print("the second parameter:");
    at_port_print(dataBuf);
    at_port_print("\r\n");
    #endif
    os_memset(sysCfg.mqtt_pass, 0, sizeof(sysCfg.mqtt_pass));
    os_strncpy(sysCfg.mqtt_pass, dataBuf, sizeof(sysCfg.mqtt_pass));
    at_response_ok();
    os_free(topicBuf);
    os_free(dataBuf);
    gIfSetDevIDAndToken = 1;
}
#elif 1
void ICACHE_FLASH_ATTR
at_setupCmdMqtidtoken(uint8_t id, char *pPara)
{
    #define AT_MAX_LEN 116
    int result = 0, err = 0, flag = 0;
    char *topicBuf = (char*)os_zalloc(128),
         *dataBuf = (char*)os_zalloc(1024);
    pPara++; // skip '='
    os_memset(topicBuf, 0, 128);
    int32 lenTopic = at_data_str_copy(topicBuf, &pPara, 127);
    //int lenTopic = os_strlen(topicBuf);
    #if 0
    at_port_print("the first parameter:");
    at_port_print(topicBuf);
    at_port_print("\r\n");
    #endif
    os_memset(sysCfg.device_id, 0, sizeof(sysCfg.device_id));
    os_strncpy(sysCfg.device_id, topicBuf, sizeof(sysCfg.device_id));
    // The user name is same as device id.
    os_memset(sysCfg.mqtt_user, 0, sizeof(sysCfg.mqtt_user));
    os_strncpy(sysCfg.mqtt_user, topicBuf, sizeof(sysCfg.mqtt_user));
    if (*pPara++ != ',') { // skip ','
        at_response_error();
        os_free(topicBuf);
        os_free(dataBuf);
        return;
    }

    // Let us get token.
    at_data_str_copy(dataBuf, &pPara, 1024);
    #if 0
    at_port_print("the second parameter:");
    at_port_print(dataBuf);
    at_port_print("\r\n");
    #endif
    os_memset(sysCfg.mqtt_pass, 0, sizeof(sysCfg.mqtt_pass));
    os_strncpy(sysCfg.mqtt_pass, dataBuf, sizeof(sysCfg.mqtt_pass));
    
    at_response_ok();
    os_free(topicBuf);
    os_free(dataBuf);
    gIfSetDevIDAndToken = 1;
}
#else
void ICACHE_FLASH_ATTR
at_setupCmdMqtidtoken(uint8_t id, char *pPara)
{
    uint8 buffer[32] = {0};
    os_sprintf(buffer, "%s\r\n", "at_setupCmdMqtidtoken");
    at_port_print(buffer);
    at_response_ok();
    }
#endif

void ICACHE_FLASH_ATTR
at_queryCmdMqtidtoken(uint8_t id)
{
    uint8 *buffer = (uint8 *)os_zalloc(128);
    os_sprintf(buffer, "DeviceID=%s,Token=%s\r\n", sysCfg.device_id, sysCfg.mqtt_pass);
    at_port_print(buffer);
    at_response_ok();
    os_free(buffer);
}

void ICACHE_FLASH_ATTR
at_setupCmdMqtsettopic(uint8_t id, char *pPara)
{
    #define AT_MAX_LEN 116
    int result = 0, err = 0, flag = 0;
    gDefaultTopic = (char*)os_zalloc(128);
    pPara++; // skip '='
    pPara++; // skip "

    int32 leftLen = AT_MAX_LEN - 2;
    if (leftLen <= 0)
    {
        at_response_error();
        os_free(gDefaultTopic);
        return;
    }
    os_memset(gDefaultTopic, 0, 128);
    int32 i = 0;
    while (*pPara != '\r')
    {
        gDefaultTopic[i] = *pPara;
        i++;
        if (i > 1024)
        {
            break;
        }
        pPara++;
    }
    int32 len = i - 1;
    gDefaultTopic[len] = 0;
    #if 0
    at_port_print("the first parameter:");
    at_port_print(gDefaultTopic);
    at_port_print("\r\n");
    #endif
    at_response_ok();
}

void ICACHE_FLASH_ATTR
at_setupCmdMqtsetmessage(uint8_t id, char *pPara)
{
    #define AT_MAX_LEN 116
    int result = 0, err = 0, flag = 0;
    gDefaultMessage = (char*)os_zalloc(128);
    pPara++; // skip '='
    pPara++; // skip "

    int32 leftLen = AT_MAX_LEN - 2;
    if (leftLen <= 0)
    {
        at_response_error();
        os_free(gDefaultMessage);
        return;
    }
    os_memset(gDefaultMessage, 0, 128);
    int32 i = 0;
    while (*pPara != '\r')
    {
        gDefaultMessage[i] = *pPara;
        i++;
        if (i > 1024)
        {
            break;
        }
        pPara++;
    }
    int32 len = i - 1;
    gDefaultMessage[len] = 0;
    #if 0
    at_port_print("the first parameter:");
    at_port_print(gDefaultMessage);
    at_port_print("\r\n");
    #endif
    at_response_ok();
}

at_funcationType at_custom_cmd[] = {
    {"+TEST", 5, at_testCmdTest, at_queryCmdTest, at_setupCmdTest, at_exeCmdTest},
    {"+LXY", 4, NULL, at_queryCmdLxy, NULL, at_exeCmdLxy},
    {"+MQTSETSER", 10, NULL, NULL, at_setupCmdMqtsetser, NULL},
    {"+MQTCON", 7, NULL, NULL, at_setupCmdMqtcon, at_exeCmdMqtcon},
    {"+MQTSUBS", 8, NULL, NULL, at_setupCmdMqtsubs, NULL},
    {"+MQTPUB", 7, NULL, NULL, at_setupCmdMqtpub, at_exeCmdMqtpub},
    {"+MQTPUBM", 8, NULL, NULL, at_setupCmdMqtpubm, NULL},
    {"+MQTDISCON", 10, NULL, NULL, NULL, at_exeCmdMqtdiscon},
    {"+MQTSTATE", 9, NULL, at_queryCmdMqtstate, NULL, NULL},

    {"+MQTSETTOPIC", 12, NULL, NULL, at_setupCmdMqtsettopic, NULL},
    {"+MQTSETMESSAGE", 14, NULL, NULL, at_setupCmdMqtsetmessage, NULL},
    
    {"+MQTIDTOKEN", 11, NULL, at_queryCmdMqtidtoken, at_setupCmdMqtidtoken, NULL},
    {"+MQTCOND", 8, NULL, at_queryCmdMqtcond, NULL, NULL},
#ifdef AT_CUSTOM_UPGRADE
    {"+CIUPDATE", 9,       NULL,            NULL,            NULL, at_exeCmdCiupdate}
#endif
};

/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABBBCDDD
 *                A : rf cal
 *                B : at parameters
 *                C : rf init data
 *                D : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
*******************************************************************************/
uint32 ICACHE_FLASH_ATTR
user_rf_cal_sector_set(void)
{
    enum flash_size_map size_map = system_get_flash_size_map();
    uint32 rf_cal_sec = 0;

    switch (size_map) {
        case FLASH_SIZE_4M_MAP_256_256:
            rf_cal_sec = 128 - 5;
            break;

        case FLASH_SIZE_8M_MAP_512_512:
            rf_cal_sec = 256 - 5;
            break;

        case FLASH_SIZE_16M_MAP_512_512:
        case FLASH_SIZE_16M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;

        case FLASH_SIZE_32M_MAP_512_512:
        case FLASH_SIZE_32M_MAP_1024_1024:
            rf_cal_sec = 1024 - 5;
            break;

        case FLASH_SIZE_64M_MAP_1024_1024:
            rf_cal_sec = 2048 - 5;
            break;
        case FLASH_SIZE_128M_MAP_1024_1024:
            rf_cal_sec = 4096 - 5;
            break;
        default:
            rf_cal_sec = 0;
            break;
    }

    return rf_cal_sec;
}

#if 1
void ICACHE_FLASH_ATTR
user_rf_pre_init(void)
{
    system_phy_freq_trace_enable(at_get_rf_auto_trace_from_flash());
}
#endif

void wifiConnectCb(uint8_t status)
{
	if(status == STATION_GOT_IP){
		MQTT_Connect(&mqttClient);
	} else {
		MQTT_Disconnect(&mqttClient);
	}
}
void mqttConnectedCb(uint32_t *args)
{
	MQTT_Client* client = (MQTT_Client*)args;
	at_port_print("MQTT: Connected\r\n");
    if (gIfEverDisconnected == 1)
    {
        gIfEverDisconnected = 0;
        if (strcmp(sysCfg.mqtt_host, "mcotton.microduino.cn") == 0) // Microduino
        {
            char *topicBuf = (char*)os_zalloc(128);
            os_sprintf(topicBuf, "ca/%s", sysCfg.device_id);
            MQTT_Subscribe(&mqttClient, topicBuf, 0);
            os_free(topicBuf);
        }
        else // OneNET
        {
            int i;
            for (i = 0; i < TOPIC_NUMBER; i++)
            {
                if (gSubTopics[i] != NULL)
                {
                    MQTT_Subscribe(&mqttClient, gSubTopics[i], 0);
                }
            }
        }
        at_port_print("Subscribed\r\n");
    }
    at_response_ok();
#if 0
	MQTT_Subscribe(client, "/mqtt/topic/0", 0);
	MQTT_Subscribe(client, "/mqtt/topic/1", 1);
	MQTT_Subscribe(client, "/mqtt/topic/2", 2);

	MQTT_Publish(client, "/mqtt/topic/0", "hello0", 6, 0, 0);
	MQTT_Publish(client, "/mqtt/topic/1", "hello1", 6, 1, 0);
	MQTT_Publish(client, "/mqtt/topic/2", "hello2", 6, 2, 0);
#endif
}

void mqttDisconnectedCb(uint32_t *args)
{
    gIfEverDisconnected = 1;
	MQTT_Client* client = (MQTT_Client*)args;
	at_port_print("MQTT: Disconnected\r\n");
    at_response_ok();
}

void mqttPublishedCb(uint32_t *args)
{
	MQTT_Client* client = (MQTT_Client*)args;
	//at_port_print("MQTT: Published\r\n");
	at_response_ok();
}

void mqttDataCb(uint32_t *args, const char* topic, uint32_t topic_len, const char *data, uint32_t data_len)
{
    char *buf = (char*)os_zalloc(MQTT_BUF_SIZE);
	char *topicBuf = (char*)os_zalloc(topic_len+1),
			*dataBuf = (char*)os_zalloc(data_len+1);
 
	MQTT_Client* client = (MQTT_Client*)args;

	os_memcpy(topicBuf, topic, topic_len);
	topicBuf[topic_len] = 0;

	os_memcpy(dataBuf, data, data_len);
	dataBuf[data_len] = 0;

    //os_sprintf(buf, "Receive topic: %s, data: %s \r\n", topicBuf, dataBuf);
    os_sprintf(buf, "%s\r\n", dataBuf);
    at_port_print(buf);
	//INFO("Receive topic: %s, data: %s \r\n", topicBuf, dataBuf);
	os_free(topicBuf);
	os_free(dataBuf);
    os_free(buf);
}

void mqttTimeoutCb(uint32_t *args)
{
    gIfEverDisconnected = 1;
    at_port_print("MQTT: Timeout\r\n");
    at_response_ok();
}

void Thebe_at_mqtt_init(void)
{
    #if 0
    uint8 staMAC[6];
    char temp[64];
    wifi_get_macaddr(STATION_IF, staMAC);
    os_sprintf(temp, "\""MACSTR"\" disconnected\r\n", MAC2STR(staMAC));
    #endif
	//MQTT_InitLWT(&mqttClient, "/lwt", "offline", 0, 0);
	MQTT_InitLWT(&mqttClient, "", "", 0, 0);
	//MQTT_InitLWT(&mqttClient, "Thebe", temp, 0, 0); // ������������
    
	MQTT_OnConnected(&mqttClient, mqttConnectedCb);
	MQTT_OnDisconnected(&mqttClient, mqttDisconnectedCb);
	MQTT_OnPublished(&mqttClient, mqttPublishedCb);
	MQTT_OnData(&mqttClient, mqttDataCb);
    MQTT_OnTimeout(&mqttClient, mqttTimeoutCb);
	//at_port_print("\r\nSystem started ...\r\n");
}

void Thebe_mqtt_init(void)
{
    //os_delay_us(60000);
    at_port_print("Enter Thebe_mqtt_init()");
    
    CFG_Load();

	MQTT_InitConnection(&mqttClient, sysCfg.mqtt_host, sysCfg.mqtt_port, sysCfg.security);
	//MQTT_InitConnection(&mqttClient, "192.168.11.122", 1880, 0);

	MQTT_InitClient(&mqttClient, sysCfg.device_id, sysCfg.mqtt_user, sysCfg.mqtt_pass, sysCfg.mqtt_keepalive, 1);
	//MQTT_InitClient(&mqttClient, "client_id", "user", "pass", 120, 1);

	//MQTT_InitLWT(&mqttClient, "/lwt", "offline", 0, 0);
	MQTT_InitLWT(&mqttClient, "", "", 0, 0);
    
	MQTT_OnConnected(&mqttClient, mqttConnectedCb);
	MQTT_OnDisconnected(&mqttClient, mqttDisconnectedCb);
	MQTT_OnPublished(&mqttClient, mqttPublishedCb);
	MQTT_OnData(&mqttClient, mqttDataCb);

	//WIFI_Connect(sysCfg.sta_ssid, sysCfg.sta_pwd, wifiConnectCb);

	//INFO("\r\nSystem started ...\r\n");
	at_port_print("\r\nSystem started ...\r\n");
}

void user_init(void)
{
#if 1
    //os_delay_us(60000);
    char buf[64] = {0};
    at_customLinkMax = 5;
    int i;
    for (i = 0; i < TOPIC_NUMBER; i++)
    {
        gSubTopics[i] = NULL;
    }
    //Thebe_mqtt_init();
    at_init();
    os_sprintf(buf,"Compile time:%s %s\r\nSoftware version:%s",__DATE__,__TIME__, SW_VERSION);
    //os_sprintf(buf, "Software version:%s", SW_VERSION);
    at_set_custom_info(buf);
    at_cmd_array_regist(&at_custom_cmd[0], sizeof(at_custom_cmd)/sizeof(at_custom_cmd[0]));
    at_port_print("\r\nready...\r\n");
#endif

#if 0
    uart_init(BIT_RATE_115200, BIT_RATE_115200);
	os_delay_us(60000);
    os_printf("\r\nready.\r\n");
#endif
    //Thebe_mqtt_init();
    //Thebe_at_mqtt_init();
    //system_init_done_cb(Thebe_mqtt_init);
}
